$(document).ready(function() {

  $('.home-slides').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
  });

  $('.home-alcostore-slides').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
  });

  $('.home-reviews').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
  });

  $('.good-slides').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    fade: true,
    cssEase: 'linear'
  });

  $('.main-mob-header-top__links-drop').click(function() {
    $('.main-mob-header-top__nav').toggle('slide');
  });

  $('.main-info__fav').click(function() {
    $('.main-info__favorites').toggle('slide');
  });

  $('.page-sort__collapser').click(function() {
    $(this).next().toggle();
  });

  $('.main-mob-custom-droper').click(function() {
    $(this).next().toggle('slide');
  });

  $('.dropdown-toggle').click(function() {
    $(this).next().toggle('slide');
  });

});